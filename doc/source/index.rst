.. telcowg-usecases documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

telcowg-usecases Design Specifications
==================================================

.. toctree::
   :glob:
   :maxdepth: 2

   usecases/*


telcowg-usecases Repository Information
===================================================

.. toctree::
   :maxdepth: 2

   README <readme>
   contributing
   workflow

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

